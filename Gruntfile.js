module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Task uglify
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },
    // Task sass
    sass: {                              
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'style/style.css': 'src/style.scss'
        }
      }
    },
    // Task watch
    watch: {
      sass: {
        files: 'src/style.scss',
        tasks: ['sass'],
        options: {
          livereload: true,
        },
      },
    },
  });

  // Load the plugins that provides the ("uglify", "watch", "sass") task(s).
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'sass', 'watch']);
};